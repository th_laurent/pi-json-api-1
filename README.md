# README

This is a simple (and incomplete by any means!) JSON REST API developed with Ruby on Rails, following the [JSON API](http://jsonapi.org/) specifications

## Installation

```
git clone https://bitbucket.org/th_laurent/pi-json-api-1
cd pi-json-api-1/
bundle install
rake db:migrate
bundle exec rails s
```

## API usage

This API follow the [JSON API](http://jsonapi.org/) specifications

* Show all users (GET /users)

```curl -i -X GET http://localhost:3000/users```

* Show specific user (GET /users/[user-id]) 

```curl -i -X GET http://localhost:3000/users/[user-id]```

* Create a user (POST /users)

```curl -i -X POST -H "Content-Type:application/vnd.api+json" http://localhost:3000/users -d '{"data":{"type":"users","attributes":{"username":"xkcd","email":"xkcd@example.com","password":"correct_horse_battery_staple"}}}'```

* Delete a user (DELETE /users/[user-id])

```curl -i -X DELETE -H "X-Api-Key: [API key]" http://localhost:3000/users/[user-id]```

The API key is configured in config/secrets.yml

## Running the tests

```rspec -fd```

The ouput should be:

```
User
  has a valid factory
  is not valid without a username
  is not valid without an email
  is not valid without a password
  is not valid if the username already exists
  is not valid if the email already exists
  is not valid if the password is too short

Users
  GET /users
    responds with status code 200
    lists all the users in the database
  GET /users/:id
    when requested user is valid
      responds with status code 200
      returns the requested user
  POST /users
    when the request is valid
      creates the user in the database
      responds with status code 201
      returns the newly created user
    when the request is invalid
      does not create the user in the database
      responds with status code 422
  DELETE /users/:id
    when the API key is valid
      when requested user is valid
        deletes the user from the database
        responds with status code 204
    when the API key is invalid
      doesn't delete the user from the database
      responds with status code 401

UsersController
  routing
    routes to #index
    routes to #show
    routes to #create
    routes to #destroy

Finished in XXX seconds (files took XXX seconds to load)
24 examples, 0 failures
```

## Improvements for the future

* API versionning (probably through Content Negotiation)

* Error serialization

* Force SSL in production

* Enforcing password strength (possibly using [strong_password](https://github.com/bdmac/strong_password))

* Request limitation/throttling (possibly using [Rack::Attack](https://github.com/kickstarter/rack-attack))

* Errors and exceptions need to be handled better

* and of course, needs a broader range of tests, especially around invalid requests and edge cases

