require 'active_model_serializers'

# Switch to the json:api adapter, which follows the format
# specified in http://jsonapi.org/format
ActiveModelSerializers.config.adapter = :json_api

api_mime_types = %W(  
  application/vnd.api+json
  text/x-json
  application/json
)
Mime::Type.register 'application/vnd.api+json', :json, api_mime_types
