class User < ApplicationRecord
  before_save { self.email = email.downcase}
  validates :username, presence: true,
                       uniqueness: { case_sensitive: false }
  validates :email, presence: true,
                    uniqueness: { case_sensitive: false }
                  # format: <= email validation based on regex is
                  # a can of worm, will avoid it for the time being
  has_secure_password
  validates :password, length: { minimum: 8 }
  # could also test password strength (entropy-based with the 'strong_password' gem)
  # https://github.com/bdmac/strong_password
end
