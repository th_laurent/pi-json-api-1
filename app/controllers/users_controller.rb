class UsersController < ApplicationController
  before_action :validate_api_key, only: :destroy
  before_action :set_user, only: [:show, :update, :destroy]

  # GET /users
  def index
    @users = User.all

    render json: @users
  end

  # GET /users/1
  def show
    render json: @user
  end

  # POST /users
  def create
    @user = User.new(user_attributes)

    if @user.save
      render json: @user, status: :created, location: @user
    else
      render json: @user.errors, status: :unprocessable_entity
    end
  end

  # DELETE /users/1
  def destroy
    @user.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def user_params
      params.require(:data).permit(:type, { attributes: [:username, :email, :password] })
    end

    def user_attributes
      user_params[:attributes] || {}
    end

    def validate_api_key
      if Rails.application.secrets.secret_key_api != request.headers['X-Api-Key']
        render json: 'Bad credentials', status: :unauthorized
      end
    end

end
