require 'rails_helper'

RSpec.describe User, type: :model do
  it "has a valid factory" do
    user = FactoryGirl.build :user
    expect(user).to be_valid
  end
  
  it "is not valid without a username" do
    user = FactoryGirl.build :user, username: nil
    expect(user).to_not be_valid
  end
  
  it "is not valid without an email" do
    user = FactoryGirl.build :user, email: nil
    expect(user).to_not be_valid
  end

  it "is not valid without a password" do
    user = FactoryGirl.build :user, password: nil
    expect(user).to_not be_valid
  end
  
  it "is not valid if the username already exists" do
    user1 = FactoryGirl.create :user, username: "bob"
    user2 = FactoryGirl.build :user, username: "bob", email: "bob2@example.com"
    expect(user2).to_not be_valid
  end
  
  it "is not valid if the email already exists" do
    user1 = FactoryGirl.create :user, email: "bob@example.com"
    user2 = FactoryGirl.build :user, email: "bob@example.com"
    expect(user2).to_not be_valid
  end
  
  it "is not valid if the password is too short" do
    user = FactoryGirl.build :user, password: '1234567'
    expect(user).to_not be_valid
  end
  
  #it "is not valid if the password is too weak"
  # could use the 'strong_password' gem to this effect
  # https://github.com/bdmac/strong_password
end
