require 'rails_helper'

RSpec.describe "Users", type: :request do

  describe "GET /users" do
    it "responds with status code 200" do
      get users_path

      expect(response).to have_http_status(200)
    end

    it "lists all the users in the database" do
      FactoryGirl.create_list :user, 3

      get users_path
      
      body = JSON.parse(response.body)
      expect(body['data'].count).to eql(3)
      # Improvement: check that the users are the correct ones
    end
  end

  describe "GET /users/:id" do
    context 'when requested user is valid' do
      subject(:user) { FactoryGirl.create :user }
      
      it "responds with status code 200" do
        get user_path(user.id)
        
        expect(response).to have_http_status(200)        
      end
      
      it "returns the requested user" do
        get user_path(user.id)
        
        expect(response).to have_http_status(200)        
      end
    end
    
    #context 'when requested user does not exist' do
    #  it "responds with status code 404" do
    #    get user_path('42')
    #
    #    expect(response).to have_http_status(404)
    #  end
    #  
    #  it "returns an error message"
    #end
  end
  
  describe "POST /users" do
    context 'when the request is valid' do
      subject(:user_params) {
        user = FactoryGirl.build :user
        {
          data: {
            type: "users",
            attributes: {
              username: user.username,
              email: user.email,
              password: user.password
            }
          }
        }
      }
      before do
        post users_path,
             params: user_params.to_json,
             headers: { 'Content-Type': 'application/vnd.api+json' }
      end
      
      it "creates the user in the database" do
        body = JSON.parse(response.body)
        user = User.find(body['data']['id'])

        expect(user).to be_present
        expect(user.username).to eq(body['data']['attributes']['username'])
        expect(user.email).to eq(body['data']['attributes']['email'])
      end
      
      it "responds with status code 201" do
        expect(response).to have_http_status(201) 
      end
      
      it "returns the newly created user" do
        body = JSON.parse(response.body)
        request = JSON.parse(user_params.to_json)
        expect(body['data']['attributes']['username']).to eq(request['data']['attributes']['username'])
      end
    end
    
    context 'when the request is invalid' do
      subject(:user_params) {
        user = FactoryGirl.build :user
        {
          data: {
            type: "users",
            attributes: {
              username: user.username,
              email: user.email,
              password: nil
            }
          }
        }
      }
      before do
        post users_path,
             params: user_params.to_json,
             headers: { 'Content-Type': 'application/vnd.api+json' }
      end
      
      it "does not create the user in the database" do
        expect(User.all.count).to eql(0)
      end
      
      it "responds with status code 422" do
        expect(response).to have_http_status(422) 
      end
      
      #it "returns an error message"
      # => will do if I have the time to do a proper error serializer 
    end
  end
  
  describe "DELETE /users/:id" do
    subject(:user) { FactoryGirl.create :user }
    context 'when the API key is valid' do
      context 'when requested user is valid' do
        it "deletes the user from the database" do
          delete  user_path(user.id),
                  headers: { 'X-Api-Key': Rails.application.secrets.secret_key_api }
        
          expect(User.all.count).to eql(0)
        end
        
        it "responds with status code 204" do
          delete  user_path(user.id),
                  headers: { 'X-Api-Key': Rails.application.secrets.secret_key_api }
        
          expect(response).to have_http_status(204)
        end
      end

      #context 'when requested user is invalid' do
      #  it "responds with status code 404"
      #  it "returns an error message"
      #end
    end

    context 'when the API key is invalid' do
      it "doesn't delete the user from the database" do
        delete  user_path(user.id)
        
        expect(User.find(user.id)).to be_present
      end
      
      it "responds with status code 401" do
        delete  user_path(user.id)
        
        expect(response).to have_http_status(401)
      end
    end
  end
  
end
