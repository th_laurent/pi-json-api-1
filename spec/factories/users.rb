FactoryGirl.define do
  # create :user will create a User object with
  # username set to user1
  # email set to user1@example.com
  # password
  factory :user do
    sequence(:username) { |n| "user#{n}" }
    email { "#{username}@example.com".downcase }
    password "correct_horse_battery_staple_;-)"
  end
end
